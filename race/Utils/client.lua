function math.lerp(from, to, t)
    return from + (to - from) * t
end