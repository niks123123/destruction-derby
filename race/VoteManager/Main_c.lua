local iTo
local aMaps
local iHaderHeight = 20
local fX, fY = guiGetScreenSize()
local fMaxLenth = 0
local fHeightForOne = 25
local fPickerMap
local aAlowedKeys = {}

function StartVote ( aMapsNames )
	aMaps = aMapsNames
	iTo = #aMaps
	for i, v in ipairs ( aMaps ) do
		local sTemp = dxGetTextWidth ( v, 1, "default-bold" )
		if sTemp > fMaxLenth then
			fMaxLenth = sTemp
		end
	end
  aAlowedKeys = {}
	for i = 1, iTo, 1 do
		bindKey ( tostring ( i ), "up", VoteForMap )
        table.insert ( aAlowedKeys, tostring ( i ) )
	end
	addEventHandler ( 'onClientRender', root, RenderChoose )
end
addEvent ( 'OnVoteStart', true )
addEventHandler ( 'OnVoteStart', root, StartVote )

function StopVote()
	for i = 1, iTo, 1 do
		unbindKey ( tostring ( i ), "up", VoteForMap )
	end
	fPickerMap = 0
	removeEventHandler ( 'onClientRender', root, RenderChoose )
end
addEvent ( 'OnVoteStop', true )
addEventHandler ( 'OnVoteStop', localPlayer, StopVote )

function VoteForMap ( sKey )
    for i, v in ipairs ( aAlowedKeys ) do
        if sKey == v then
	       triggerServerEvent ( 'VoteForMap', localPlayer, sKey )
        end
    end
	fPickerMap = tonumber ( sKey )
end

function RenderChoose()
	local fPosX = fX - fMaxLenth - 15
	local fPosY = fY - iTo * fHeightForOne
	local fStartX = fPosX + 3
	local fStartY = fPosY - 20
	dxDrawRectangle ( fPosX, fPosY, fMaxLenth + 6, iTo * fHeightForOne, tocolor ( 0, 0, 0, 150 ), false )
	for i, v in ipairs ( aMaps ) do
		local sText
		if i == fPickerMap then
			sText = "#FF0000"..v
		else
			sText = v
		end
		dxDrawColorText ( i..". "..sText, fStartX, fStartY + i * fHeightForOne, 0, 0, tocolor ( 255, 255, 255, 255 ), 1, "default-bold" )
	end
end
-- Usefull functions
function dxDrawColorText(str, ax, ay, bx, by, color, scale, font, alignX, alignY)
  bx, by, color, scale, font = bx or ax, by or ay, color or tocolor(255,255,255,255), scale or 1, font or "default"
  if alignX then
    if alignX == "center" then
      ax = ax + (bx - ax - dxGetTextWidth(str:gsub("#%x%x%x%x%x%x",""), scale, font))/2
    elseif alignX == "right" then
      ax = bx - dxGetTextWidth(str:gsub("#%x%x%x%x%x%x",""), scale, font)
    end
  end
  if alignY then
    if alignY == "center" then
      ay = ay + (by - ay - dxGetFontHeight(scale, font))/2
    elseif alignY == "bottom" then
      ay = by - dxGetFontHeight(scale, font)
    end
  end
  local alpha = string.format("%08X", color):sub(1,2)
  local pat = "(.-)#(%x%x%x%x%x%x)"
  local s, e, cap, col = str:find(pat, 1)
  local last = 1
  while s do
    if cap == "" and col then color = tocolor(getColorFromString("#"..col..alpha)) end
    if s ~= 1 or cap ~= "" then
      local w = dxGetTextWidth(cap, scale, font)
      dxDrawText(cap, ax, ay, ax + w, by, color, scale, font)
      ax = ax + w
      color = tocolor(getColorFromString("#"..col..alpha))
    end
    last = e + 1
    s, e, cap, col = str:find(pat, last)
  end
  if last <= #str then
    cap = str:sub(last)
    dxDrawText(cap, ax, ay, ax + dxGetTextWidth(cap, scale, font), by, color, scale, font)
  end
end