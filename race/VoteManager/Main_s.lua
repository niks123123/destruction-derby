_DEBUG = true

class 'CVotemanager'
{
	m_pCurrentMap = false;
	m_iPickNumber = 0;
	m_pTimer = false;
	m_aMapsPick = {};
	m_aMapsForVote = {};
	m_iMapPlayed = 0;
	m_iRoundsForMap = 2;
	m_bWorked = false;
};

function SetCurrentMap ( pResource )
	if CVotemanager.m_pCurrentMap then
		stopResource ( CVotemanager.m_pCurrentMap )
		CVotemanager.m_iMapPlayed = 0
	end
	local sType = getResourceInfo ( pResource, 'type' )
	if sType == 'map' then
		CVotemanager.m_pCurrentMap = pResource
		CVotemanager.m_iMapPlayed = CVotemanager.m_iMapPlayed + 1
	end
end
addEventHandler ( "onResourceStart", root, SetCurrentMap )

function CVotemanager:GetCurrentMap()
	return self.m_pCurrentMap
end

function CVotemanager:GetWorkState()
	return self.m_bWorked
end

function CVotemanager:GetAllMaps()
	local aAllMaps = {}
	local aResources = getResources()
	for i, v in ipairs ( aResources ) do
		local sGameMode = getResourceInfo ( v, 'gamemodes' )
		local sType = getResourceInfo ( v, 'type' )
		if sType == 'map' and sGameMode == 'race' then
			table.insert ( aAllMaps, v )
		end
	end
	return aAllMaps
end

function CVotemanager:PickRandomMaps ( iNumber )
	local aAllMaps = CVotemanager:GetAllMaps()
	local aMaps = {}
	local iTo = #aAllMaps
	local iCounter = 1
	while ( iCounter <= iNumber ) do
		local iRandom = math.random ( 1, iTo )
		if aAllMaps[iRandom] and aAllMaps[iRandom] ~= self.m_pCurrentMap then
			table.insert ( aMaps, aAllMaps[iRandom] )
			table.remove ( aAllMaps, iRandom )
			iCounter = iCounter + 1
		end
	end
	return aMaps
end

function CVotemanager:StartVote()
	if self.m_bWorked == false then
		self.m_bWorked = true
		local aPlayers = GetPlayersWithState ( 'Dead' )
		for i, v in ipairs ( aPlayers ) do
			setElementData ( v, 'votemanager.voted', false )
		end
		self.m_iPickNumber = 0
		self.m_aMapsPick = {};
		self.m_aMapsForVote = {};
		local aMaps = CVotemanager:PickRandomMaps ( 5 )
		CVotemanager.m_aMapsForVote = aMaps
		local aMapsNames = {}
		for i, v in ipairs ( aMaps ) do
			aMapsNames[i] = getResourceInfo ( v, 'name' )
		end
		if self.m_iMapPlayed <= self.m_iRoundsForMap and self.m_pCurrentMap then
			table.insert ( aMapsNames, 'Again' )
			self.m_iMapPlayed = self.m_iMapPlayed + 1
		else
			self.m_iMapPlayed = 0
		end
		triggerClientEvent ( 'OnVoteStart', root, aMapsNames )
		CVotemanager.m_pTimer = setTimer (
			function()
				self.m_bWorked = false
				triggerClientEvent ( 'OnVoteStop', root )
				local index = table.maxn ( CVotemanager.m_aMapsPick )
				if index == 0 then
					index = math.random ( 1, 5 )
				end
				if index ~= 0 then
					if index == 6 then
						CVotemanager:StartMap ( CVotemanager.m_pCurrentMap )
						if _DEBUG then
							outputChatBox ( "ПО ВРЕМЕНИ AGAIN" )
						end
						return
					end
					CVotemanager:StartMap ( CVotemanager.m_aMapsForVote[index] )
				end
				if _DEBUG then
					outputChatBox ( "ПО ВРЕМЕНИ "..index )
				end
			end, 10000, 1 )
	end
end

function VoteHandler ( sKey )
	local aPlayers = getElementsByType ( 'player' )
	local i = tonumber ( sKey )
	local iChoose = getElementData ( source, 'votemanager.voted' )
	if not iChoose then
		CVotemanager.m_iPickNumber = CVotemanager.m_iPickNumber + 1
		if not CVotemanager.m_aMapsPick[i] then
			CVotemanager.m_aMapsPick[i] = 1
		else
			CVotemanager.m_aMapsPick[i] = CVotemanager.m_aMapsPick[i] + 1
		end
		setElementData ( source, 'votemanager.voted', i )
	else
		CVotemanager.m_aMapsPick[iChoose] = CVotemanager.m_aMapsPick[iChoose] - 1
		if not CVotemanager.m_aMapsPick[i] then
			CVotemanager.m_aMapsPick[i] = 1
		else
			CVotemanager.m_aMapsPick[i] = CVotemanager.m_aMapsPick[i] + 1
		end
		setElementData ( source, 'votemanager.voted', i )
	end
	if #aPlayers == CVotemanager.m_iPickNumber then
		CVotemanager.m_bWorked = false
		if isTimer ( CVotemanager.m_pTimer ) then
			killTimer ( CVotemanager.m_pTimer )
		end
		triggerClientEvent ( 'OnVoteStop', root )
		local index = table.maxn ( CVotemanager.m_aMapsPick )
		if not i then
				i = math.random ( 1, 5 )
			end
		if index ~= 0 then
			if index == 6 then
				CVotemanager:StartMap ( CVotemanager.m_pCurrentMap )
				if _DEBUG then
					outputChatBox ( "ПО ПИКУ AGAIN" )
				end
				return 
			end
			CVotemanager:StartMap ( CVotemanager.m_aMapsForVote[index] )
		end
		if _DEBUG then
			outputChatBox ( "ПО ПИКУ "..index )
		end
	end
end
addEvent ( 'VoteForMap', true )
addEventHandler ( 'VoteForMap', root, VoteHandler )

function CVotemanager:StartMap ( pMap )
	startResource ( pMap )
	local sMapName = getResourceInfo ( pMap, 'name' )
	local aPlayers = getElementsByType ( 'player' )
	for i, v in ipairs ( aPlayers ) do
		fadeCamera ( v, false, 0 )
		setElementData ( v, 'votemanager.voted', false )
	end
	outputChatBox ( 'Map #FFFF00"'..sMapName..'"#FFFFFF started.', root, 255, 255, 255, true )
	CRace:SetState ( 1, pMap )
end

function CVotemanager:StopCurrentMap()
	if self.m_pCurrentMap then
		stopResource ( self.m_pCurrentMap )
	end
end

function _f1()
	CVotemanager:StartVote()
end
addCommandHandler ( "startvote", _f1 )