local iNitroModel = 2221
local iRepairModel = 2222
local iCarchangeMod = 2223
local iColShapeSize = 3.5

function LoadimgMapHandler ( aArg )
	if CRace:GetState() == 1 then
		local aColShape = getElementsByType ( 'colshape' )
		if aColShape then
			for i, v in ipairs ( aColShape ) do
				destroyElement ( v )
			end
		end
		local sResourceName = getResourceName ( aArg[1] ) 
		local a = Parsing ( sResourceName, 'racepickup' )
		triggerClientEvent ( 'ChangeRotation', root, a )
		local iTo = #a
		for i = 1, iTo, 1 do
			if a[i].type == 'nitro' then
				local pShape = createColSphere ( a[i].posX, a[i].posY, a[i].posZ, iColShapeSize )
				addEventHandler ( 'onColShapeHit', pShape, TakeNitroFunction )
			elseif a[i].type == 'repair' then
				local pShape = createColSphere ( a[i].posX, a[i].posY, a[i].posZ, iColShapeSize )
				addEventHandler ( 'onColShapeHit', pShape, RepairFunction )
			elseif a[i].type == 'vehiclechange' then
				local pShape = createColSphere ( a[i].posX, a[i].posY, a[i].posZ, iColShapeSize )
				setElementData ( pShape, 'ddrace.pickup.vehToChange', a[i].vehicleToChange )
				addEventHandler ( 'onColShapeHit', pShape, ChangecarFunction )
			end
		end
	end
end
addEvent ( 'OnRaceChangeState', true )
addEventHandler ( 'OnRaceChangeState', resourceRoot, LoadimgMapHandler )

function TakeNitroFunction ( pHitElement, bMatchDimenstion )
	if getElementType ( pHitElement ) == 'vehicle' and bMatchDimenstion then
		addVehicleUpgrade ( pHitElement, 1010 )
	end
	if getElementType ( pHitElement ) == 'player' and bMatchDimenstion then
		playSoundFrontEnd ( pHitElement, 46 )
	end
end

function RepairFunction ( pHitElement, bMatchDimenstion )
	if getElementType ( pHitElement ) == 'vehicle' and bMatchDimenstion then
		fixVehicle ( pHitElement )
	end
	if getElementType ( pHitElement ) == 'player' and bMatchDimenstion then
		playSoundFrontEnd ( pHitElement, 46 )
	end
end

function ChangecarFunction ( pHitElement, bMatchDimenstion )
	if getElementType ( pHitElement ) == 'vehicle' and bMatchDimenstion then
		local iR, iG, iB, iR1, iG1, iB1 = getVehicleColor ( pHitElement )
		local iModel = getElementData ( source, 'ddrace.pickup.vehToChange' )
		getElementData ( pHitElement, 'ddrace.pickup.vehToChange' )
		setElementModel ( pHitElement, tonumber ( iModel ) )
		setVehicleColor ( pHitElement, iR, iG, iB, iR1, iG1, iB1 )
	end
	if getElementType ( pHitElement ) == 'player' and bMatchDimenstion then
		playSoundFrontEnd ( pHitElement, 46 )
	end
end