local aChangeModels = { 2221, 2222, 2223 }
local aChangeModelsNames = { { 'Files/Models/nitro.dff', 'Files/Models/nitro.txd' },
							 { 'Files/Models/repair.dff', 'Files/Models/repair.txd' },
							 { 'Files/Models/vehiclechange.dff', 'Files/Models/vehiclechange.txd' } }
local aObjects = {}
local iTo = 0
local iStartTick = getTickCount()

local function StartHandler()
	for i, v in ipairs ( aChangeModels ) do
		pTXD = engineLoadTXD ( aChangeModelsNames[i][2] )
		engineImportTXD ( pTXD, v )
		pDFF = engineLoadDFF ( aChangeModelsNames[i][1] )
		engineReplaceModel ( pDFF, v )
	end
end
addEventHandler ( 'onClientResourceStart', resourceRoot, StartHandler )

function ChangeObjectsRotationEvent ( a )
	aObjects = {}
	iTo = #a
	for i = 1, iTo, 1 do
		if a[i].type == 'nitro' then
			local pObject = createObject ( aChangeModels[1], a[i].posX, a[i].posY, a[i].posZ )
			setElementCollisionsEnabled ( pObject, false )
			table.insert ( aObjects, pObject )
		elseif a[i].type == 'repair' then
			local pObject = createObject ( aChangeModels[2], a[i].posX, a[i].posY, a[i].posZ )
			setElementCollisionsEnabled ( pObject, false )
			table.insert ( aObjects, pObject )
		elseif a[i].type == 'vehiclechange' then
			local pObject = createObject ( aChangeModels[3], a[i].posX, a[i].posY, a[i].posZ )
			setElementCollisionsEnabled ( pObject, false )
			table.insert ( aObjects, pObject )
		end
	end
end
addEvent ( 'ChangeRotation', true )
addEventHandler ( 'ChangeRotation', root, ChangeObjectsRotationEvent )

function ChangeObjectsRotation()
	local fAngle = math.fmod ( ( getTickCount() - iStartTick ) * 360 / 2000, 360 )
	for i, v in ipairs ( aObjects ) do
		if v then
			setElementRotation ( v, 0, 0, fAngle )
		end
	end
end
addEventHandler ( 'onClientRender', root, ChangeObjectsRotation )