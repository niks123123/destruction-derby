local sMapName = ''
local sMapAuthor = ''
local sResourceName = ''
local sDownloadedMapName = ''
local iAlpha = 0
local iScreenWidth, iScreenHeight = guiGetScreenSize()
local aTimers = {}

function ShowInformation ( sName, sAuthor, sRName )
	sMapName = sName
	sMapAuthor = sAuthor
	sResourceName = sRName
	iAlpha = 255
	addEventHandler ( "onClientRender", root, ShowInformation )

end
addEvent ( 'ShowMapInformation', true )
addEventHandler ( 'ShowMapInformation', root, ShowInformation )

function ShowInformation()
	if sMapName then
		local fNameWidth = dxGetTextWidth ( sMapName, 2.5, 'bankgothic' )
		dxDrawText ( sMapName, iScreenWidth/2 - fNameWidth/2, iScreenHeight/2 - 100, 0, 0, tocolor(255, 160, 0, iAlpha), 2.5, 'bankgothic' )
	else
		local fNameWidth = dxGetTextWidth ( sResourceName, 2.5, 'bankgothic' )
		dxDrawText ( sResourceName, iScreenWidth/2 - fNameWidth/2, iScreenHeight/2 - 100, 0, 0, tocolor(255, 160, 0, iAlpha), 2.5, 'bankgothic' )
	end
	if sMapAuthor then
		local fAuthorWidth = dxGetTextWidth ( sMapAuthor, 2.5, 'bankgothic')
		dxDrawText ( sMapAuthor, iScreenWidth/2 - fAuthorWidth/2, iScreenHeight/2 + 50, 0, 0, tocolor(255, 160, 0, iAlpha), 2.5, 'bankgothic' )
	else
		local fAuthorWidth = dxGetTextWidth ( 'No author', 2.5, 'bankgothic')
		dxDrawText ( 'No author', iScreenWidth/2 - fAuthorWidth/2, iScreenHeight/2 + 50, 0, 0, tocolor(255, 160, 0, iAlpha), 2.5, 'bankgothic' )
	end
end

function HideInformation()
	removeEventHandler ( "onClientRender", root, ShowInformation )
end
addEvent ( 'HideMapInformation', true )
addEventHandler ( 'HideMapInformation', root, HideInformation )

function EventStopDownloadContent()
	triggerServerEvent ( 'OnPlayerEndedDownloadContent', localPlayer )
end
addEventHandler ( 'onClientResourceStart', root, EventStopDownloadContent )

function EventStartDownloadMap ( DownloadedMapName )
	sDownloadedMapName = DownloadedMapName
end
addEvent ( 'OnMapStartDownload', true )
addEventHandler ( 'OnMapStartDownload', root, EventStartDownloadMap )

function EventStopDownloadMap ( pResource )
	if getResourceName ( pResource ) == sDownloadedMapName then
		triggerServerEvent ( 'OnMapStopDownload', localPlayer, sDownloadedMapName )
	end
end
addEventHandler ( 'onClientResourceStart', root, EventStopDownloadMap )

function AddEventToCountdown()
	if aTimers then
		for i, v in ipairs ( aTimers ) do
			if isTimer ( v ) then
				killTimer ( v )
			end
		end
	end
	aTimers = {}
	for i = 1000, 4000, 1000 do
		local pTimer = setTimer ( function() outputChatBox ( i/1000 ) end, i, 1 )
		table.insert ( aTimers, pTimer )
	end
end
addEvent ( 'StartCountdown', true )
addEventHandler ( 'StartCountdown', localPlayer, AddEventToCountdown )