class 'CRace'
{
	m_iIndexOfSpawn = 1;
	m_iState = 0;
	m_sVersion = '0.0';
	m_aAspawnPoints = {};
	m_aPickups = {};
};

function CRace:SetState ( iID, ... )
	self.m_iState = iID
	local aArg = { ... }
	triggerEvent ( 'OnRaceChangeState', root, aArg )
end

function CRace:GetState()
	return self.m_iState
end

function CRace:SetIndexOfSpawn ( iInt )
	self.m_iIndexOfSpawn = iInt
end

function CRace:GetIndexOfSpawn()
	return self.m_iIndexOfSpawn
end

function CRace:GetStateName()
	if self.m_iState == 0 then
		return 'No Map'
	elseif self.m_iState == 1 then
		return 'Loading Map'
	elseif self.m_iState == 2 then
		return 'Countdown'
	elseif self.m_iState == 3 then
		return 'Running'
	elseif self.m_iState == 4 then
		return 'Someone Won'
	elseif self.m_iState == 5 then
		return 'Next Map Vote'
	end
end

function CRace:GetVersion()
	local pVersion = getResourceInfo ( getThisResource(), 'version' )
	self.m_Version = pVersion
end

function StartHandler()
	CRace:GetVersion()
	CRace:SetState ( 0 )
	setFPSLimit ( 60 )
	exports.scoreboard:scoreboardAddColumn ( 'State' )
	local aPlayers = getElementsByType ( 'aPlayers' )
	print ( '[DDRACE]: Restruction Rerby v'..CRace.m_Version..' started!' )  
end
addEventHandler ( 'onResourceStart', resourceRoot, StartHandler )

function MapResourceHandler ( pResource )
	local sType = getResourceInfo ( pResource, 'type' )
	if sType == 'map' then
		local sMapName = getResourceName ( pResource )
		triggerClientEvent ( 'OnMapStartDownload', root, sMapName )
		CRace:SetIndexOfSpawn ( 1 )
	end
end
addEventHandler ( 'onResourceStart', root, MapResourceHandler )

function JoinHandler()
	SetPlayerState ( source, 'Downloading Content' )
end
addEventHandler ( 'onPlayerJoin', root, JoinHandler )

function QuitHandler()
	local aPlayers = getElementsByType ( 'player' )
	if #aPlayers == 1 then
		CRace:SetState ( 0 )
	end
end
addEventHandler ( 'onPlayerQuit', root, QuitHandler )

function PressKeyToDieFunction ( pPlayer )
	if ( GetPlayerState ( pPlayer ) == 'Alive' ) and CRace:GetState() == 3 then
		KillPlayer ( pPlayer )
	end
end

function KillPlayer ( pPlayer )
	local pVehicle = getPedOccupiedVehicle ( pPlayer )
	if pPlayer and pVehicle then
		killPed ( pPlayer)
		setTimer ( function ( Player ) setElementPosition ( Player, 0, 0, 0 )  SetPlayerSpactator ( Player ) end, 50, 1, pPlayer )
		destroyElement ( pVehicle )
		SetPlayerState ( pPlayer, 'Dead' )
	end
end

function SetPlayerSpactator ( pPlayer )
	local aPlayers = GetAlivePlayers()
	if #aPlayers > 1 then
		local iRandom = math.random ( 1, #aPlayers )
		setCameraTarget ( pPlayer, aPlayers[iRandom] )
	elseif #aPlayers == 1 then
		setCameraTarget ( pPlayer, aPlayers[1] )
		CRace:SetState ( 4 )
	elseif #aPlayers == 0 then
		CRace:SetState ( 5 )
		local aPlayers = GetPlayersWithState ( 'Dead' )
		for i, v in ipairs ( aPlayers ) do
			fadeCamera ( v, false, 0 )
		end
	end
end

function GetAlivePlayers()
	return GetPlayersWithState ( 'Alive' )
end

function StopDonwloadContentHandler()
	SetPlayerState ( source, 'Dead' )
	local aPlayers = getElementsByType ( 'player' )
	if CRace:GetState() == 0 then
		if #aPlayers == 1 then
			CVotemanager:StartVote()
		end
	end
end
addEvent ( 'OnPlayerEndedDownloadContent', true )
addEventHandler ( 'OnPlayerEndedDownloadContent', root, StopDonwloadContentHandler )

function StopDonwloadMapHandler ( sMapName )
	CRace.m_aAspawnPoints = Parsing ( sMapName, 'spawnpoint' );
	local i = CRace:GetIndexOfSpawn()
	local a = CRace.m_aAspawnPoints
	local pVehicle = createVehicle ( a[i].vehicle, a[i].posX, a[i].posY, a[i].posZ, 0, 0, a[i].rotation )
	setElementPosition( source, a[i].posX, a[i].posY, a[i].posZ );
	spawnPlayer ( source, a[i].posX, a[i].posY, a[i].posZ, 0, 0, 0, 0 )
	toggleControl ( source, 'enter_exit', false )
	bindKey ( source, 'enter', 'up', PressKeyToDieFunction, source )
	setElementFrozen ( pVehicle, true )
	setElementCollisionsEnabled ( pVehicle, false )
	setElementSyncer ( pVehicle, false )
	setCameraTarget ( source, source )
	SetPlayerState ( source, 'Alive' )
	local i = i + 1
	CRace:SetIndexOfSpawn ( i )
	setTimer ( function ( Player, Vehicle ) warpPedIntoVehicle ( Player, Vehicle ) end, 1000, 1, source, pVehicle )
	setTimer (
		function ( pPlayer )
			triggerClientEvent ( 'HideMapInformation', root )
			fadeCamera ( pPlayer, true )
			local aPlayers = GetPlayersWithState ( 'Downloading Map' )
			if #aPlayers == 0 and CRace:GetState() ~= 2 then
				CRace:SetState ( 2 )
			end
		end, 2000, 1, source )
end
addEvent ( 'OnMapStopDownload', true )
addEventHandler ( 'OnMapStopDownload', root, StopDonwloadMapHandler )

function DDRaceStateFunctions ( aArg )
	print ( CRace:GetStateName() )
	if CRace:GetState() == 0 then
		CVotemanager:StopCurrentMap()
	elseif CRace:GetState() == 1 then
		local sMapName = getResourceInfo ( aArg[1], 'name' )
		local sAuthor = getResourceInfo ( aArg[1], 'author' )
		local sResourceName = getResourceName ( aArg[1] )
		outputChatBox ( tostring ( sMapName )..' '..tostring ( sAuthor ) )
		triggerClientEvent ( 'ShowMapInformation', root, sMapName, sAuthor, sResourceName )
		local aObjects = getElementsByType ( 'object' )
		for i, v in ipairs ( aObjects ) do
			if v ~= 2221 and v ~= 2222 and v ~= 2223 then
				setElementCollisionsEnabled ( v, true )
			end
		end
	elseif CRace:GetState() == 2 then
		local aPlayers = GetPlayersWithState ( 'Alive' )
		for i, v in ipairs ( aPlayers ) do
			triggerClientEvent ( v, 'StartCountdown', root )
			local Vehicle = getPedOccupiedVehicle ( v )
			setTimer ( function 
						( pVehicle ) setElementCollisionsEnabled ( pVehicle, true ) 
						setElementFrozen ( pVehicle, false )
						CRace:SetState ( 3 )
					end, 4000, 1, Vehicle )
		end
	elseif CRace:GetState() == 4 then
		outputChatBox ( 'вызвали голосование со стейта 4' )
		CRace:SetState ( 5 )
		if CVotemanager:GetWorkState() == false then
			CVotemanager:StartVote()
		end
	elseif CRace:GetState() == 5 then
		outputChatBox ( 'вызвали голосование со стейта 5' )
		if CVotemanager:GetWorkState() == false then
			CVotemanager:StartVote()
		end
	end
end
addEvent ( 'OnRaceChangeState', true )
addEventHandler ( 'OnRaceChangeState', root, DDRaceStateFunctions )

function SetPlayerState ( pPlayer, sState )
	setElementData ( pPlayer, 'State', sState )
	triggerEvent ( 'OnPlayerChangeState', root, pPlayer, sState )
end

function GetPlayerState ( pPlayer )
	local sState = getElementData ( pPlayer, 'State' )
	return sState
end

function PlayerStateFunction ( pPlayer, sState )
end
addEvent ( 'OnPlayerChangeState', true )
addEventHandler ( 'OnPlayerChangeState', root, PlayerStateFunction )

function GetPlayersWithState ( sState )
	local aPlayers = getElementsByType ( 'player' )
	local aTable = {}
	for i, v in ipairs ( aPlayers ) do
		if GetPlayerState ( v ) == sState then
			table.insert ( aTable, v )
		end
	end
	if aTable then
		return aTable
	else
		return false
	end
end
